##
## Velocity Template for DisplayAttributeReleasePage view-state
##
## Velocity context will contain the following properties :
##
## attributeReleaseContext - context holding consentable attributes
## attributeReleaseFlowDescriptor - attribute consent flow descriptor
## attributeDisplayNameFunction - function to display attribute name
## attributeDisplayDescriptionFunction - function to display attribute description
## consentContext - context representing the state of a consent flow
## encoder - HTMLEncoder class
## flowExecutionKey - SWF execution key (this is built into the flowExecutionUrl)
## flowExecutionUrl - form action location
## flowRequestContext - Spring Web Flow RequestContext
## profileRequestContext - OpenSAML profile request context
## request - HttpServletRequest
## response - HttpServletResponse
## rpUIContext - context with SP UI information from the metadata
## environment - Spring Environment object for property resolution
#set ($serviceName = $rpUIContext.serviceName)
#set ($serviceDescription = $rpUIContext.serviceDescription)
#set ($informationURL = $rpUIContext.informationURL)
#set ($privacyStatementURL = $rpUIContext.privacyStatementURL)
#set ($rpOrganizationLogo = $rpUIContext.getLogo())
#set ($rpOrganizationName = $rpUIContext.organizationName)
#set ($replaceDollarWithNewline = true)
##
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="$request.getContextPath()/css/base.css">
        <link rel="stylesheet" type="text/css" href="$request.getContextPath()/css/application.css">
        <title>#springMessageText("idp.attribute-release.title", "Information Release")</title>
    </head>
    <body id="corp" class="has-aside col-corp">
    	<div id="header-bg"></div>
        <div id="wrapper">

            <header id="header">
                <div id="header-logo">
                    <div id="header-logo-web">
                        <a href="https://www.ethz.ch"><img alt="ETH Homepage" src="$request.getContextPath()/images/eth_logo.png" /></a>
                    </div>
                    <div id="header-logo-print">
                        <p id="claim">
                            <img src="$request.getContextPath()/images/eth_logo_kurz_schwarz_D_print_300dpi.png" border="0" title="Print" alt="#springMessageText("idp.logo.alt-text", "logo")">
                        </p>
                    </div>
                </div>
            </header>

            <main id="main">

                <!-- breadcrumb -->
                <nav id="nav-flow">
                    <ul>
                        <li>
                            <a>#springMessageText("idp.login.breadcrumb", "ETH Zurich Login")</a>
                        </li>
                    </ul>
                </nav>

                <section id="content">

                    <div id="content-top">
                        <h1>#springMessageText("idp.attribute-release.title", "Information Release")</h1>
                    </div>

                    <div id="content-main">

                        <form action="$flowExecutionUrl" method="post" style="padding:10px">

                            <div class="ce-box">
                                <p>#springMessageText("idp.attribute-release.serviceNameLabel", "You are about to access the service:")<br>

                                    #set ($serviceName = $rpUIContext.serviceName)
                                    #if ($serviceName && !$rpContext.getRelyingPartyId().contains($serviceName))
                                        <strong>
                                            $encoder.encodeForHTML($serviceName)
                                        </strong>
                                    #end
                                    #springMessageText("idp.attribute-release.of", "of")
                                    #set ($ourl=$rpUIContext.organizationURL)
                                    #set ($odn=$rpUIContext.organizationDisplayName)
                                    #if ($ourl && $odn)
                                        <a href="$encoder.encodeForHTMLAttribute($ourl)" iconoption="AUTO" target="_blank">$encoder.encodeForHTML($odn)<span class="icon">&nbsp;</span></a>
                                    #end
                                </p>
                                #set ($serviceDescription = $rpUIContext.serviceDescription)
                                #if ($serviceDescription && !$rpContext.getRelyingPartyId().contains($serviceDescription))
                                    <p>#springMessageText("idp.attribute-release.serviceDescriptionLabel","Description as provided by this service:")<br>
                                        <em>
                                            $encoder.encodeForHTML($serviceDescription)
                                        </em>
                                    </p>
                                #end
                                #if ($informationURL)
                                    <p style="margin-top: 10px;">
                                        <a href="$informationURL" iconoption="AUTO" target="_blank">#springMessageText("idp.attribute-release.informationURLLabel", "Additional information about the service")<span class="icon">&nbsp;</span></a>
                                    </p>
                                #end
                            </div>

                            <div id="attributeRelease">
                                <h2>#springMessageText("idp.attribute-release.attributesHeader", "Information to be Provided to Service")</h2>
                                <div class="scrollarea">
                                    <div class="scrollarea-main">
                                        <div class="scrollarea-scroll-window">
                                            <div class="scrollarea-content">
                                                <table class="attributeTable">
                                                    <tbody>
                                                        #foreach ($attribute in $attributeReleaseContext.getConsentableAttributes().values())
                                                            <tr>
                                                                <td>$encoder.encodeForHTML($attributeDisplayNameFunction.apply($attribute))</td>
                                                                <td>
                                                                    #foreach ($value in $attribute.values)
                                                                        #if ($replaceDollarWithNewline)
                                                                            #set ($encodedValue = $encoder.encodeForHTML($value.getDisplayValue()).replaceAll($encoder.encodeForHTML("$"),"<br>"))
                                                                        #else
                                                                            #set ($encodedValue = $encoder.encodeForHTML($value.getDisplayValue()))
                                                                        #end
                                                                        #if ($attributeReleaseFlowDescriptor.perAttributeConsentEnabled)
                                                                            <label for="$attribute.id">$encodedValue</label>
                                                                        #else
                                                                            $encodedValue
                                                                        #end
                                                                        <br>
                                                                    #end
                                                                </td>
                                                                <td style="vertical-align: top">
                                                                    #if ($attributeReleaseFlowDescriptor.perAttributeConsentEnabled)
                                                                        #set ($inputType = "checkbox")
                                                                    #else
                                                                        #set ($inputType = "hidden")
                                                                    #end
                                                                    <input id="$attribute.id" type="$inputType" name="_shib_idp_consentIds" value="$encoder.encodeForHTML($attribute.id)" checked>
                                                                </td>
                                                            </tr>
                                                        #end
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            #if ($privacyStatementURL)
                                <p style="margin-top: 10px;">
                                    <a href="$privacyStatementURL" iconoption="AUTO" target="_blank">#springMessageText("idp.attribute-release.privacyStatementURLLabel", "Data privacy information of the service")<span class="icon">&nbsp;</span></a>
                                </p>
                            #end
                            <div class="ce-text">
                                <p>
                                    #springMessageText("idp.attribute-release.confirmationQuestion", "The information above would be shared with the service if you proceed. Do you agree to release this information to the service every time you access it?")
                                </p>
                                <div class="ce-box">
                                    #if ($attributeReleaseFlowDescriptor.doNotRememberConsentAllowed || $attributeReleaseFlowDescriptor.globalConsentAllowed)
                                        <p>
                                        #springMessageText("idp.attribute-release.consentMethod", "Select an information release consent duration:")
                                        </p>
                                    #end
                                    #if ($attributeReleaseFlowDescriptor.doNotRememberConsentAllowed)
                                            <div class="consentDuration">
                                                <input id="_shib_idp_doNotRememberConsent" type="radio" name="_shib_idp_consentOptions" value="_shib_idp_doNotRememberConsent">
                                                <label for="_shib_idp_doNotRememberConsent">#springMessageText("idp.attribute-release.doNotRememberConsent", "Ask me again at next login")</label>
                                                <p>#springMessageText("idp.attribute-release.doNotRememberConsentItem", "I agree to send my information this time.")</p>
                                            </div>
                                        #end
                                    #if ($attributeReleaseFlowDescriptor.doNotRememberConsentAllowed || $attributeReleaseFlowDescriptor.globalConsentAllowed)
                                            <div class="consentDuration">
                                            <input id="_shib_idp_rememberConsent" type="radio" name="_shib_idp_consentOptions" value="_shib_idp_rememberConsent" checked>
                                                <label for="_shib_idp_rememberConsent">#springMessageText("idp.attribute-release.rememberConsent", "Ask me again if information changes")</label>
                                                <p>#springMessageText("idp.attribute-release.rememberConsentItem", "I agree that the same information will be sent automatically to this service in the future.")</p>
                                            </div>
                                        #end
                                        #if ($attributeReleaseFlowDescriptor.globalConsentAllowed)
                                            <div class="consentDuration">
                                                <input id="_shib_idp_globalConsent" type="radio" name="_shib_idp_consentOptions" value="_shib_idp_globalConsent">
                                                <label for="_shib_idp_globalConsent">#springMessageText("idp.attribute-release.globalConsent", "Do not ask me again")</label>
                                                <p>#springMessageText("idp.attribute-release.globalConsentItem", "I agree that <strong>all</strong> of my information will be released to <strong>any</strong> service.")</p>
                                            </div>
                                        #end
                                    #if ($attributeReleaseFlowDescriptor.doNotRememberConsentAllowed || $attributeReleaseFlowDescriptor.globalConsentAllowed)
                                        #springMessageText("idp.attribute-release.consentMethodRevoke", "This setting can be revoked at any time with the checkbox on the login page.")
                                    #end
                                    <div class="consentDuration">
                                        <input type="submit" name="_eventId_AttributeReleaseRejected" value="#springMessageText("idp.attribute-release.reject", "Reject")" style="margin-right: 30px;">
                                        <input type="submit" name="_eventId_proceed" value="#springMessageText("idp.attribute-release.accept", "Accept")">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </main>
        </div>
    </body>
</html>
